<?php
require_once 'users/init.php';
if(isset($user) && $user->isLoggedIn()){
    //echo '<script>console.log("username:' . echousername($user->data()->id) . '");</script>';
    echo '<script>console.log("userid:' . echouserid($user->data()->id) . '");</script>';
}
// TODO drop this auth mechanism?? -> replace with simple php scripts.. 
?>

    <!DOCTYPE html>
    <html>

    <head>
        <meta charset='utf-8' />
        <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        
        
        <script type="text/javascript">
        function updateLocation(data)
        {
            return true;
        }
        </script>
        
        
        
        <link href='styles.css' rel='stylesheet' />
        <link href='../fullcalendar.min.css' rel='stylesheet' />
        <link href='../fullcalendar.print.min.css' rel='stylesheet' media='print' />

        <script src='../lib/moment.min.js'></script>
        <script src='../lib/jquery.min.js'></script>
        <script src='../lib/jquery-ui.min.js'></script>
        <script src='../fullcalendar.min.js'></script>

        <script src='locale-all.js'></script>
        <script src='theme-chooser.js'></script>

        <script src="jquery.ui.touch.js"></script>
        <script src="jquery.ui.touch-punch.min.js"></script>

        <link rel="stylesheet" href="bootstrap.min.css">
        <script src="popper.min.js"></script>
        <script src="bootstrap.min.js"></script>

        <script src="slideout.min.js"></script>

        <script src="calApp_object.js"></script>
        
        <span id="userID" hidden>0</span>

        <?php
            if(isset($user) && $user->isLoggedIn()){
                echo '<script>calApp.userid =' . echouserid($user->data()->id) . ';//console.log(calApp.userid);</script>';
            }
        ?>

            <script src="main.js"></script>

            <style>
                
                html {
                    /* Status bar height on iOS 10 */
                    padding-top: 100px;
                    /* Status bar height on iOS 11.0 */
                    padding-top: constant(safe-area-inset-top);
                    /* Status bar height on iOS 11+ */
                    padding-top: env(safe-area-inset-top);
                }
                
                
                body {
                    margin-top: 40px !important;
                }
                
                .panel {
                    height: 95% !important;
                }
                
                
                .alert-info,
                .alert-info>th,
                .alert-info>td {
                    background-color: #a9d8cb61 !important;
                }

            </style>
    </head>

    <body>
        
        <!--<span id="userID">0</span>-->

        <nav id="menu" class="menu">
            <a>
                <header class="menu-header">
                    <h1 class="title menu-header-title"><i class="fa fa-edit fa-1x"></i> menu </h1>
                </header>
            </a>

            <section class="menu-section">
                <h3 class="menu-section-title">Aplikacja:</h3>
                <ul class="menu-section-list">
                    <li><a onclick="calApp.show('wrap')">Kalendarz wydarzeń</a></li>
                    <!-- //TODO wykres
                    <li><a onclick="calApp.show('chart'); calApp.calculateSleepTime(calApp.userid)">Wykres</a></li>
                    -->
                </ul>
            </section>
            

            <!--
        <section class="menu-section">
            <h3 class="menu-section-title">Funkcje:</h3>
            <ul class="menu-section-list">
                <li><a onclick="show('fn1')">.....</a></li>
                <li><a onclick="saveView()">.....</a></li>
            </ul>
        </section>

        <section class="menu-section">
            <h3 class="menu-section-title">Opcje</h3>
            <ul class="menu-section-list">
                <li><a onclick="show('opt1')">Ustawienia aplikacji</a></li>
            </ul>
        </section>
        -->

            <section class="menu-section">
                <h3 class="menu-section-title">Info:</h3>
                <ul class="menu-section-list">
                    <li><a onclick="calApp.show('credits')">Informacje</a></li>
                    <li><a onclick="calApp.show('contact')">Kontakt</a></li>
                </ul>
            </section>






        </nav>

        <main id="panel" class="panel">
            <!--
            <header class="panel-header">
                <button type="button" class="btn btn-primary toggle-button">☰ Menu</button> &nbsp;
                <h1 class="title"><i class="fa fa-edit fa-1x"></i> baby notes </h1>
            </header>
            -->

            <div id='wrap' class="shown">

                <div id='calendar'></div>

                <div style='clear:both'></div>

                <div id='bottom' style="display: none;">
                    <div class='left'>
                        <div id='theme-system-selector' class='selector' style="display: none;">
                            Theme System:
                            <select>
                    <option value='bootstrap4' selected>Bootstrap 4</option>
                    <option value='standard'>unthemed</option>
                </select>
                        </div>
                        <div data-theme-system="bootstrap4" class='selector' style='display:none'>
                            Motyw kolorystyczny:
                            <select>
                        <option value='minty' selected>Minty</option>
                        <option value='slate'>Slate</option>
                        <option value='superhero'>Superhero</option>
                    </select>
                        </div>
                        <span id='loading' style='display:none'>loading theme...</span>
                    </div>

                    <div class='right'>
                    </div>

                    <div class='clear'></div>
                </div>

            </div>

            <div class="fullscreen hidden" id="chart">
                <h2>Wykresy aktywności</h2>
                <div class="ct-chart"></div>
            </div>

            <div class="fullscreen hidden" id="credits">
                <h2>O aplikacji</h2>
                <p>Aplikacja jest prostym notatnikiem / kalendarzem / dziennikiem aktywności Twojego dziecka. Zapisuj kiedy dziecko śpi lub kiedy zostało nakarmione - może Ci to ułatwić analizę aktywności Twojego dziecka i przewidzieć następne wydarzenia. <br>Aplikacja zaplanowana jest głównie dla monitorowania i prowadzenia statystyk aktywności noworodka i małego dziecka.</p>
                <h2>Projekt</h2>
                <p>Aplikacja zaprojektowana i zaprogramowana w oparciu o biblioteki open-source: <a href="http://fullcalendar.io/">Fullcalendar.io</a> wraz z pluginami, <a href="https://slideout.js.org/">Slideout.js</a>. </p>
                <h2>Autor</h2>
                <p>Aplikacja zaprojektowana i stworzona przez:
                    <p>Projekt, produkcja i programowanie: <a href="www.dagmarazawada.pl">dagmarazawada.pl</a></p>
                    <p>Zgłaszanie błędów, mail: <a href="mailto:dagmara.zawada@gmail.com">dagmara.zawada@gmail.com</a></p>
            </div>

            <div class="fullscreen hidden" id="contact">
                <h2>Kontakt</h2>
                <p>Projekt, produkcja i programowanie: <a href="www.dagmarazawada.pl">dagmarazawada.pl</a></p>
                <p>Zgłaszanie błędów, mail: <a href="mailto:dagmara.zawada@gmail.com">dagmara.zawada@gmail.com</a></p>
            </div>

        </main>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="eventModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Dodaj wpis</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="start-date" class="col-form-label">Data:</label>
                                <input type="date" class="form-control" id="start-date">
                                <input type="time" class="form-control" id="start-time" style="width:49%; display:inline;">
                                <!--<label for="end-date" class="col-form-label">End:&nbsp;&nbsp;</label>
                            <input type="date" class="form-control" id="end-date" style="width:50%; display:inline;">-->
                                <input type="time" class="form-control" id="end-time" style="width:49%; display:inline;">
                            </div>
                            <div class="form-group">
                                <label for="event-name" class="col-form-label">Wydarzenie:</label>
                                <!--<input type="text" class="form-control" id="event-name">-->
                                <select id="event-name">
                                <option value='karmienie'>Karmienie</option>
                                <option value='sen'>Sen</option>
                                <option value='pieluszka'>Pieluszka</option>
                                <option value='placz'>Płacz</option>
                                <option value='inne'>Inne</option>
                            </select>
                            </div>
                            <div class="form-group">
                                <label for="description" class="col-form-label">Dodatkowy opis:</label>
                                <textarea class="form-control" id="description"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                        <button type="button" class="btn btn-primary" id="saveEvent">Zapisz</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="eventModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Wydarzenie: <span class="eventTitle"></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="description" class="col-form-label">Dodatkowy opis:</label>
                                <span class="eventDescription">description</span>
                            </div>
                            <div class="form-group">
                                <label for="start-date" class="col-form-label">Data:</label>
                                <input type="date" class="form-control start-date" readonly>
                                <input type="time" class="form-control start-time" style="width:49%; display:inline;" readonly>
                                <input type="time" class="form-control end-time" style="width:49%; display:inline;" readonly>
                            </div>
                            
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="introModal" tabindex="-1" role="dialog" aria-labelledby="introModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Użytkownik niezalogowany</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <p>Do pełnej funkcjonalności aplikacji zaloguj się lub utworz nowe konto w menu po lewej stronie.</p>
                        <p>Tylko zalogowani użytkownicy mogą zapisywać wydarzenia i odczytywać je z bazy danych.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="dataSavedModal" tabindex="-1" role="dialog" aria-labelledby="dataSavedModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Dane zapisane</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <p>Wydarzenie zapisane do bazy danych!</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                    </div>
                </div>
            </div>
        </div>

    </body>

    <script>
        $(document).ready(function() {
            
            var userID = $("#userID");
            
            $("#saveEvent").click(function() {
                var userID = $("#userID").html();
                //alert(userID);
                calApp.userid=userID;
                $.ajax(
                            {
                            type: "POST",
                            url: "saveDataDB.php",
                            data:   {"userid":userID
                                                , "start-date": $("#start-date").val()
                                                , "start-time": $("#start-time").val()
                                                , "end-date": $("#end-date").val()
                                                , "end-time": $("#end-time").val()
                                                , "event-name": $("#event-name").val()
                                                , "description": $("#description").val()
                                    },
                            success: function(data) {
                                //alert("saved", data);
                                $("#calendar").fullCalendar( "refetchEvents" );
                                calApp.openDataSavedModal();
                            }
                            });

                $('#myModal').modal('hide');

            });

            setTimeout(function() {
                calApp.makeTodaybtnActive();
                $('button.fc-agenda-button').text('3dni');
                $('button.fc-today-button').click(function() { //TODO
                    console.log('today btn clicked');
                    calApp.makeTodaybtnActive();
                });

            }, 400);
            
            


            var chartData = {
                labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                series: [
                    [5, 4, 3, 7, 5, 10, 3],
                    [3, 2, 9, 5, 4, 6, 4]
                ]
            };

            var chartOptions = {
                width: 600,
                height: 500,
                seriesBarDistance: 10,
                reverseData: true,
                horizontalBars: true,
                axisY: {
                    offset: 70
                }
            };

            var chartResponsiveOptions = [
                ['screen and (max-width: 640px)', {
                    seriesBarDistance: 5,
                    axisX: {
                        labelInterpolationFnc: function(value) {
                            return value[0];
                        }
                    }
                }]
            ];

            new Chartist.Bar('.ct-chart', chartData, chartOptions, chartResponsiveOptions);



        });

    </script>


    <link rel="stylesheet" href="chartist.min.css">
    <script src="chartist.min.js"></script>

    </html>

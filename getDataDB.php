<?php
require_once 'creds.php';
$dbname = "calendar_db";


try {
    $DBcon = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
    $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $ex) {
        die($ex->getMessage());
}

$userid = htmlspecialchars($_POST["userid"]);
$query = "SELECT * FROM `calendar_events` WHERE userid = " . $userid . ";";
//$query = "SELECT * FROM `calendar_events` WHERE userid = 11;";

$sth = $DBcon->prepare($query);
$sth->execute();

if ($sth->rowCount() > 0) {
   //echo 'ok';
}
else {
    
}

$rows = array();
while ($data = $sth->fetchAll(PDO::FETCH_ASSOC)) {    
    foreach ($data as $events => $e) {
        $temp = array(
            'title' => $e['event_name'],
            'start' => $e['start_date'] . "T" .  $e['start_time'],
            'end' => $e['end_date'] . "T" .  $e['end_time'],
            'description' => $e['description']
        );
        array_push($rows, $temp);
    }
}
echo json_encode($rows);

?>

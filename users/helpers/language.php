<?php
/*
UserSpice 4
An Open Source PHP User Management System
by the UserSpice Team at http://UserSpice.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
%m1% - Dymamic markers which are replaced at run time by the relevant index.
*/

$lang = array();

// Signup
$lang = array_merge($lang,array(
	"SIGNUP_TEXT"			=> "Zarejestruj",
	"SIGNUP_BUTTONTEXT"		=> "Zarejestruj mnie",
	"SIGNUP_AUDITTEXT"		=> "Zarejestrowano",
	));

// Signin
$lang = array_merge($lang,array(
	"SIGNIN_FAIL"			=> "** FAILED LOGIN **",
	"SIGNIN_TITLE"			=> "Proszę zaloguj się",
	"SIGNIN_TEXT"			=> "Logowanie",
	"SIGNOUT_TEXT"			=> "Wyloguj się",
	"SIGNIN_BUTTONTEXT"		=> "Login",
	"SIGNIN_AUDITTEXT"		=> "Zalogowano",
	"SIGNOUT_AUDITTEXT"		=> "Wylogowano",
	));

//Navigation
$lang = array_merge($lang,array(
	"NAVTOP_HELPTEXT"		=> "Help",
	));

//Account
$lang = array_merge($lang,array(
	"ACCOUNT_USER_ADDED" 		=> "Dodano nowego użytkownika!",
	"ACCOUNT_SPECIFY_USERNAME" 		=> "Wpisz swoją nazwę użytkownika",
	"ACCOUNT_SPECIFY_PASSWORD" 		=> "Wpisz hasło",
	"ACCOUNT_SPECIFY_EMAIL"			=> "Wpisz adres e-mail",
	"ACCOUNT_INVALID_EMAIL"			=> "Niepoprawny adres email",
	"ACCOUNT_USER_OR_EMAIL_INVALID"		=> "Niepoprawna nazwa użytkownika lub email",
	"ACCOUNT_USER_OR_PASS_INVALID"		=> "Niepoprawna nazwa użytkownika lub hasło",
	"ACCOUNT_USER_OR_PASS_INVALID2"		=> "Dokonaliśmy upgrade do lepszego zabezpieczenia Twojego konta.<a href='forgot-password.php'> Kliknij tutaj żeby wprowadzić nowe hasło do swojego konta.</a> Przepraszamy za niedogodności.",
	"ACCOUNT_ALREADY_ACTIVE"		=> "Twoje konto zostało już aktywowane",
	"ACCOUNT_INACTIVE"			=> "Twoje konto jest nieaktywne. Sprawdź swoją skrzynkę email lub folder spam w poszukiwaniu maila z instrukcjami aktywacji konta",
	"ACCOUNT_USER_CHAR_LIMIT"		=> "Twoja nazwa użytkownika musi mieć między %m1% a %m2% znakow długości",
	"ACCOUNT_DISPLAY_CHAR_LIMIT"		=> "Twoja nazwa użytkownika musi mieć między %m1% a %m2% znakow długości",
	"ACCOUNT_PASS_CHAR_LIMIT"		=> "Twoje hasło musi mieć pomiędzy %m1% a %m2% znakow długości",
	"ACCOUNT_TITLE_CHAR_LIMIT"		=> "Tytuł musi mieć między %m1% a %m2% znakow długości",
	"ACCOUNT_PASS_MISMATCH"			=> "Twoje hasło i potwierdzenie hasła muszą być takie same",
	"ACCOUNT_DISPLAY_INVALID_CHARACTERS"	=> "Wyświetlana nazwa może zawierać tylko znaki alfa-numeryczne",
	"ACCOUNT_USERNAME_IN_USE"		=> "Nazwa użytkownika %m1% jest już zajęta",
	"ACCOUNT_DISPLAYNAME_IN_USE"		=> "Wyświetlana nazwa %m1% jest już zajęta",
	"ACCOUNT_EMAIL_IN_USE"			=> "Email %m1% jest już zarejestrowany",
	"ACCOUNT_LINK_ALREADY_SENT"		=> "Wiadomość z linkiem aktywacyjnym został już wysłany na ten adres email w ciągu ostatniej %m1% godzin(y)",
	"ACCOUNT_NEW_ACTIVATION_SENT"		=> "Wysłaliśmy nową wiadomość z linkiem aktywacyjnym, proszę sprawdź swoj email",
	"ACCOUNT_SPECIFY_NEW_PASSWORD"		=> "Wpisz swoje nowe hasło",
	"ACCOUNT_SPECIFY_CONFIRM_PASSWORD"	=> "Potwierdź swoje nowe hasło",
	"ACCOUNT_NEW_PASSWORD_LENGTH"		=> "Nowe hasło musi mieć pomiędzy %m1% a %m2% znakow długości",
	"ACCOUNT_PASSWORD_INVALID"		=> "Obecne hasło nie odpowiada temu przechowywanemu w naszej bazie",
	"ACCOUNT_DETAILS_UPDATED"		=> "Szczegoly konta zaktualizowane",
	"ACCOUNT_ACTIVATION_MESSAGE"		=> "Musisz aktywować swoje konto przed logowaniem. Kliknij poniższy link do aktywacji swojego konta. \n\n
	%m1%activate-account.php?token=%m2%",
	"ACCOUNT_ACTIVATION_COMPLETE"		=> "Pomyślnie aktywowano konto. Możesz teraz się zalogować <a href=\"login.php\">here</a>.",
	"ACCOUNT_REGISTRATION_COMPLETE_TYPE1"	=> "Pomyślnie zarejestrowano nowego użytkownika. Możesz teraz się zalgować <a href=\"login.php\">here</a>.",
	"ACCOUNT_REGISTRATION_COMPLETE_TYPE2"	=> "Pomyślnie zarejestrowano nowego użytkownika. Wkrotce otrzymasz wiadomość aktywacyjną. Musisz aktywować swoje konto zanim się zalogujesz.",
	"ACCOUNT_PASSWORD_NOTHING_TO_UPDATE"	=> "Nowe hasło musi się rożnić od poprzedniego.",
	"ACCOUNT_PASSWORD_UPDATED"		=> "Zaktualizowano hasło konta",
	"ACCOUNT_EMAIL_UPDATED"			=> "Zaktualizowano adres e-mail",
	"ACCOUNT_TOKEN_NOT_FOUND"		=> "Token nie istnieje / Konto zostało już aktywowane",
	"ACCOUNT_USER_INVALID_CHARACTERS"	=> "Nazwa użytkownika może zawierać tylko znaki alfanumeryczne",
	"ACCOUNT_DELETIONS_SUCCESSFUL"		=> "Pomyślnie usunięto %m1% użytkiownikow",
	"ACCOUNT_MANUALLY_ACTIVATED"		=> "Konto %m1% zostało manualnie aktywowane",
	"ACCOUNT_DISPLAYNAME_UPDATED"		=> "Wyświetlana nazwa zmieniona na %m1%",
	"ACCOUNT_TITLE_UPDATED"			=> "Tytuł %m1% zmieniono na %m2%",
	"ACCOUNT_PERMISSION_ADDED"		=> "Dodano dostęp do poziomow uprawnień dla %m1%",
	"ACCOUNT_PERMISSION_REMOVED"		=> "Usunięto dostęp do poziomow uprawnień dla %m1%",
	"ACCOUNT_INVALID_USERNAME"		=> "Niepoprawna nazwa użytkownika",
	"CAPTCHA_ERROR"		=> "Oblałeś test captcha, robocie!",
	"USER_PROTECTION"		=> "Użytkownik jest chroniony %m1%",
	"USER_DEV_OPTION"		=> "Uzytkownik jest użytkownikiem dev %m1%",
	));

//Configuration
$lang = array_merge($lang,array(
	"CONFIG_NAME_CHAR_LIMIT"		=> "Site name must be between %m1% and %m2% characters in length",
	"CONFIG_URL_CHAR_LIMIT"			=> "Site name must be between %m1% and %m2% characters in length",
	"CONFIG_EMAIL_CHAR_LIMIT"		=> "Site name must be between %m1% and %m2% characters in length",
	"CONFIG_ACTIVATION_TRUE_FALSE"		=> "Email activation must be either `true` or `false`",
	"CONFIG_ACTIVATION_RESEND_RANGE"	=> "Activation Threshold must be between %m1% and %m2% hours",
	"CONFIG_LANGUAGE_CHAR_LIMIT"		=> "Language path must be between %m1% and %m2% characters in length",
	"CONFIG_LANGUAGE_INVALID"		=> "There is no file for the language key `%m1%`",
	"CONFIG_TEMPLATE_CHAR_LIMIT"		=> "Template path must be between %m1% and %m2% characters in length",
	"CONFIG_TEMPLATE_INVALID"		=> "There is no file for the template key `%m1%`",
	"CONFIG_EMAIL_INVALID"			=> "The email you have entered is not valid",
	"CONFIG_INVALID_URL_END"		=> "Please include the ending / in your site's URL",
	"CONFIG_UPDATE_SUCCESSFUL"		=> "Your site's configuration has been updated. You may need to load a new page for all the settings to take effect",
	));

//Forgot Password
$lang = array_merge($lang,array(
	"FORGOTPASS_INVALID_TOKEN"		=> "Your activation token is not valid",
	"FORGOTPASS_NEW_PASS_EMAIL"		=> "We have emailed you a new password",
	"FORGOTPASS_REQUEST_CANNED"		=> "Lost password request cancelled",
	"FORGOTPASS_REQUEST_EXISTS"		=> "There is already a outstanding lost password request on this account",
	"FORGOTPASS_REQUEST_SUCCESS"		=> "We have emailed you instructions on how to regain access to your account",
	));

//Mail
$lang = array_merge($lang,array(
	"MAIL_ERROR"				=> "Fatal error attempting mail, contact your server administrator",
	"MAIL_TEMPLATE_BUILD_ERROR"		=> "Error building email template",
	"MAIL_TEMPLATE_DIRECTORY_ERROR"		=> "Unable to open mail-templates directory. Perhaps try setting the mail directory to %m1%",
	"MAIL_TEMPLATE_FILE_EMPTY"		=> "Template file is empty... nothing to send",
	));

//Miscellaneous
$lang = array_merge($lang,array(
	"CAPTCHA_FAIL"				=> "Failed security question",
	"CONFIRM"				=> "Potwierdź",
	"DENY"					=> "Zaprzecz",
	"SUCCESS"				=> "Sukces",
	"ERROR"					=> "Error",
	"NOTHING_TO_UPDATE"			=> "Nie ma nic do aktualizacji",
	"SQL_ERROR"				=> "Fatal SQL error",
	"FEATURE_DISABLED"			=> "Funkcja wyłączona",
	"PAGE_PRIVATE_TOGGLED"			=> "Strona jest obecnie %m1%",
	"PAGE_ACCESS_REMOVED"			=> "Page access removed for %m1% permission level(s)",
	"PAGE_ACCESS_ADDED"			=> "Page access added for %m1% permission level(s)",
	"PAGE_REAUTH_TOGGLED"			=>  "This page %m1% verification",
	"PAGE_RETITLED"			=>  "This page has been retitled to '%m1%'",
	));


	$lang = array_merge($lang,array(
	    "MESSAGE_ARCHIVE_SUCCESSFUL"        => "You have successfully archived %m1% threads",
	    "MESSAGE_UNARCHIVE_SUCCESSFUL"      => "You have successfully unarchived %m1% threads",
	    "MESSAGE_DELETE_SUCCESSFUL"         => "You have successfully deleted %m1% threads",
			"USER_MESSAGE_EXEMPT"         			=> "User is %m1% exempted from messages.",
	    ));

//Permissions
$lang = array_merge($lang,array(
	"PERMISSION_CHAR_LIMIT"			=> "Permission names must be between %m1% and %m2% characters in length",
	"PERMISSION_NAME_IN_USE"		=> "Permission name %m1% is already in use",
	"PERMISSION_DELETIONS_SUCCESSFUL"	=> "Successfully deleted %m1% permission level(s)",
	"PERMISSION_CREATION_SUCCESSFUL"	=> "Successfully created the permission level `%m1%`",
	"PERMISSION_NAME_UPDATE"		=> "Permission level name changed to `%m1%`",
	"PERMISSION_REMOVE_PAGES"		=> "Successfully removed access to %m1% page(s)",
	"PERMISSION_ADD_PAGES"			=> "Successfully added access to %m1% page(s)",
	"PERMISSION_REMOVE_USERS"		=> "Successfully removed %m1% user(s)",
	"PERMISSION_ADD_USERS"			=> "Successfully added %m1% user(s)",
	"CANNOT_DELETE_NEWUSERS"		=> "You cannot delete the default 'new user' group",
	"CANNOT_DELETE_ADMIN"			=> "You cannot delete the default 'admin' group",
	));

	//Admin Page
	$lang = array_merge($lang,array(
		"PERMISSION_CHAR_LIMIT"			=> "Permission names must be between %m1% and %m2% characters in length",

		));



?>

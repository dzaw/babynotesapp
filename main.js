$(document).ready(function () {

    /* initialize the calendar
    -----------------------------------------------------------------*/

    var currentTime = new Date().getUTCHours() + 2;
    //console.log(currentTime); 

    initThemeChooser({
        init: function (themeSystem) {
            $('#theme-system-selector').val('bootstrap4');
            var currentTime = moment().format("HH") + ":00:00";
            $('#calendar').fullCalendar({
                locale: 'pl',
                themeSystem: themeSystem,
                height: 'parent',
                defaultView: 'agenda',
                scrollTime: currentTime,
                slotDuration: '00:15:00',
                slotLabelFormat: 'h:mm a',
                selectable: true,
                select: function (startDate, endDate) {
                    //console.log('select');
                    //alert('selected ' + startDate.format() + ' to ' + endDate.format());
                    //$('#myModal').modal('show', startDate.format(), endDate.format());
                    calApp.openModal('myModal', startDate.format(), endDate.format());
                },
                nowIndicator: true,
                //firstDay: 0,
                //dayNamesShort: ['poniedziałek', 'wtorek', 'środa', 'czwartek', 'piątek', 'sobota', 'niedziela'],
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'agenda,agendaDay,listWeek,month'
                },
                footer: {
                    left: 'prev,next today',
                    right: 'agenda,agendaDay,listWeek,month'
                },
                navLinks: true, // can click day/week names to navigate views
                editable: false,
                droppable: false, // this allows things to be dropped onto the calendar
                drop: function () {
                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }
                },
                eventSources: [
                {
                    url: 'getDataDB.php',
                    type: 'POST',
                    //data: {
                        //userid: calApp.userid
                    //},
                    data: function() { // a function that returns an object
                      return {
                        userid: parseInt(calApp.getUserID())
                      };
                    },
                    success: function(data) {
                        //console.log(data);
                        calApp.makeTodaybtnActive();
                    },
                    error: function() {
                        console.log('there was an error while fetching events!');
                        calApp.makeTodaybtnActive();
                        //calApp.openIntroModal('introModal');
                        //alert('Zaloguj się lub utworz konto żeby pobrać dane z bazy');
                    }
                    //,color: 'yellow',   // a non-ajax option
                    //textColor: 'black' // a non-ajax option
                }],
                //dayClick: function (date) {
                    //alert('clicked ' + date.format());
                    //$('#myModal').modal('show');
                //    calApp.openModal('myModal', date.format(), date.add(30, 'm').format());
                //},
                
                dayClick: function(date, allDay, jsEvent, view) { //TODO???
                    console.log('dayclick');
                    if (allDay) {
                        // Clicked on the entire day
                        $('#calendar')
                            .fullCalendar('changeView', 'agendaDay'// or 'basicDay' 
                                            )
                            .fullCalendar('gotoDate',
                                date.getFullYear(), date.getMonth(), date.getDate());
                    }
                },
                
                eventRender: function (event, element) {
                    
                    //console.log(event.description);
                    //console.log(event);
                    //console.log(element.find(".fc-title"));
                    //console.log(element.html());
                    
                    //console.log( element.find(".fc-title").text() );
                    
                    
                    if (event.title == "sen" ) {
                        element.find(".fc-title").html("SEN");
                        element.css('color', 'white');
                        element.css('background-color', '#3a87ad;');
                    }
                    if (event.title == "karmienie" ) {
                        element.find(".fc-title").html("KARMIENIE");
                        element.css('color', 'white');
                        element.css('background-color', '#bc2b08;');
                    }
                    if (event.title == "pieluszka" ) {
                        element.find(".fc-title").html("PIELUSZKA");
                        element.css('color', 'white');
                        element.css('background-color', '#4d2815;');
                    }
                    if (event.title == "inne" ) {
                        element.find(".fc-title").html("INNE");
                        element.css('color', 'white');
                        element.css('background-color', '#488911;');
                    }
                    
                    if (event.description.length > 0){
                        element.find(".fc-title").append('<span> - '+event.description+'</span>');
                    }
                    
                    //$(element).addTouch();
                    //element.draggable();
                    $(element).on("click", function(){
                        //console.log("CLICKED", event.title, event.description, event.start.format(), event.end.format());
                        calApp.openInfoModal(event.title, event.description, event.start.format(), event.end.format());
                    });
                    
                    /*
                    $(element).popover({
                        title: event.title,
                        content: event.description,
                        trigger: 'hover',
                        placement: 'top',
                        container: 'body'
                    });
                    */
                    
                },
                eventAfterRender: function (event, element, view) {
                    //element.draggable();
                },
                eventClick: function (calEvent, jsEvent, view) {
                    // change the border color just for fun
                    $(this).css('border-color', '#002c60');
                    //calApp.openModal(); //TODO open existing data modal with delete option??
                },
                eventMouseover: function (event, jsEvent, view) {
                    $(this).css('border-color', '#005960');
                    $(this).css('border-width', '0.2em');
                },
                eventMouseout: function (event, jsEvent, view) {
                    $(this).css('border-color', '');
                    $(this).css('border-width', 'thin');
                },
                visibleRange: function (currentDate) {
                    return {
                        start: currentDate.clone().subtract(1, 'days'),
                        end: currentDate.clone().add(2, 'days') // exclusive end
                    };
                }
            });
        },
        change: function (themeSystem) {
            $('#calendar').fullCalendar('option', 'themeSystem', themeSystem);
        }
    });
    
    calApp.makeTodaybtnActive();
    
    /* Slideout menu */

    calApp.initSlideout(); 
    
});

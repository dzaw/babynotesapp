Witaj w aplikacji babyNotes. Kontynuując używanie aplikacji zgadzasz się na przestrzeganie poniższych warunków użytkownika, które wraz z naszą polityką prywatności regulują relacje pomiędzy użytkownikami a niniejszą aplikacją. 

Użytkowanie aplikacji babyNotes podlega następującym warunkom:

Zawartość aplikacji jest wyłącznie do prywatnego wykorzystania przez użytkowników. Aplikacja może podlegać zmianom bez wcześniejszego powiadomienia. 

Aplikacja wykorzystuje ciasteczka do monitorowania preferencji przeglądania przez użytkowników. Jeśli zgadzasz się na wykorzystanie ciasteczek, następujące prywatne informacje mogą być zbierane przez nas do dalszego wykorzystania.

Aplikacja nie zapewnia żadnej gwarancji w odniesieniu do działania, dokładności, wydajności, spojności prezentowanych danych i materiałów pokazywanych lub oferowanych przez aplikację dla jakiegokolwiek konkretnego celu.

Użytkownik przyjmuje do wiadomości, że prezentowane w aplikacji dane i materiały mogą zawierać niedokładności, niespojności i błędy oraz, że tworcy aplikacji wyłączają swoją odpowiedzialność za tego typu niedokładości lub błedy w pełnym zakresie prawnym.

Użytkownik wykorzystuje wszelkie informacje i materiały zawarte w aplikacji całkowicie na swoje ryzyko, za ktore tworcy aplikacji nie biorą odpowiedzialności. Na użytkowniku spoczywa obowiązek zapewnienia, że produkt, usługi lub informacje dostępne przez niniejszą aplikację spełniają jego wyspecyfikowane oczekiwania.

Aplikacja zawiera materiały, ktore są własnością lub są licencjonowane przez jej tworcow. Materiały te obejmują, ale nie ograniczają się wyłącznie do designu, szablonow, stylow, wyglądu i grafik. Reprodukcja w sposob niezgodny z informacją o prawach autorskich jest zabroniona. Wszystkie znaki towarowe zawarte w tej aplikacji, ktore nie sa własnością lub nie są licencjonowane przez tworcow aplikacji, są wyszczegolnione w informacjach o aplikacji. 

Nieautoryzowane wykorzystanie aplikacji może spowodować roszczenie o odszkodowanie i/lub być przestępstwem.

Aplikacja może też zawierać odniesienia do innych treści. Odniesienia te są wprowadzone dla wygody użytkownikow i zapewnienia im dalszych informacji. Nie oznacza to, że tworcy aplikacji je aprobują lub wspierają. Tworcy aplikajci nie biorą odpowiedzialności za zawartość linkowanych treści. 
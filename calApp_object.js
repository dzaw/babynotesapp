var calApp = {
    userid: null,
    slideout: null,
    initSlideout: function () {
        calApp.slideout = new Slideout({
            'panel': document.getElementById('panel'),
            'menu': document.getElementById('menu'),
            'padding': 256,
            'tolerance': 70
        });

        $(".toggle-button").click(function () {
            calApp.slideout.toggle();
            if (calApp.slideout.isOpen()) {
                $(".panel").css("height", "100%");
            } else {
                $(".panel").css("height", "80%");
            }

        });
    },
    getUserID(){
        //console.log(calApp.userid);
        if (calApp.userid != null) {
            //console.log("returning callApp.userid");
            return calApp.userid;
        }
        else {
            //console.log("returning #userID");
            var ret = $('#userID').html();
            return ret;
        }
    },    
    getCalData(userID){
        calApp.userid = userID;
        $("#calendar").fullCalendar( "refetchEvents" );
        //alert("get cal data: " + userID);
    },
    openModal: function (modal, startDate, endDate) {
        $('#myModal').modal('show');
        var sDate = startDate.substring(0, 10);
        var sTime = startDate.substring(11, 16);
        var eDate = endDate.substring(0, 10);
        var eTime = endDate.substring(11, 16);

        //console.log(dateObj);
        //var newDateObj = moment(dateObj).add(30, 'm').toDate();
        //console.log(newDateObj.format());

        $('#start-date').val(sDate);
        $('#end-date').val(eDate);
        $('#start-time').val(sTime);
        $('#end-time').val(eTime);
    },
    openInfoModal: function(title, description, startDate, endDate) {
        $('#infoModal').modal('show');
        
        var sDate = startDate.substring(0, 10);
        var sTime = startDate.substring(11, 16);
        var eDate = endDate.substring(0, 10);
        var eTime = endDate.substring(11, 16);
        
        $('.start-date').val(sDate);
        $('.end-date').val(eDate);
        $('.start-time').val(sTime);
        $('.end-time').val(eTime);
        
        $('.eventTitle').text(title);
        $('.eventDescription').text(description);
    },
    openIntroModal: function () {
        $('#introModal').modal('show');
    },
    openDataSavedModal: function () {
        $('#dataSavedModal').modal('show');
    },
    saveData: function (data) {
        console.log(data);
    },
    show: function (toShow) {
        var toHide = document.getElementsByClassName('shown');
        var len = toHide.length;
        for (i = 0; i < len; i++) {
            toHide[i].style.display = 'none';
            toHide[i].classList.add("hidden");
            toHide[i].classList.remove("shown");
        }
        var toShow = document.getElementById(toShow);
        toShow.style.display = 'block';
        toShow.classList.add("shown");
        toShow.classList.remove("hidden");

        calApp.slideout.close();

        return false;

    },
    makeTodaybtnActive: function () {
        $('button.fc-today-button').removeAttr('disabled');
        $('button.fc-today-button').removeClass('disabled');
        $('button.fc-today-button').removeClass('fc-state-disabled');
        //console.log('btn active');
    },
    getChartData: function (userid, eventType) {
        $.ajax({
            type: 'POST',
            url: 'getChartData.php',
            data: {
                userid: userid,
                eventType: eventType
            },
            success: function (data) {
                calApp.chartData = JSON.parse(data);
                //console.log(data);
            }
        });
    },
    calculateSleepTime: function (userid) {
        console.log('calc sleep time');
        calApp.getChartData(userid, 'sen');
        setTimeout(function () {
            console.log(calApp.chartData);
            for (i in calApp.chartData) {
                var start = moment(calApp.chartData[i].start);
                var end = moment(calApp.chartData[i].end);
                var duration = moment.duration(end.diff(start)).minutes();
                
                console.log(duration);
            }
        }, 500);

    }
}

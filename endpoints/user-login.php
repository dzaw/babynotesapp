<?php
    
require('db_conn.php');
$DBname = "auth";

//$email = "dagmara.223@gmail.com";
//$pass = "123qweasd";

$email = $_POST['email'];
$pass = $_POST['pass'];

$hash = password_hash($pass, PASSWORD_BCRYPT, array('cost' => 12));

try{
        $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname;charset=utf8",$DBuser,$DBpass); //charset needed for json_encode!!
        $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "connected";
}
catch(PDOException $ex){
        die($ex->getMessage());
}

// test query
$query = "SELECT * FROM `users` WHERE email='".$email."';";

$sth = $DBcon->prepare($query);
$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_ASSOC);
//var_dump($result);
$passhash = $result[0]['password'];
//echo password_verify('123qweasd');

if (password_verify($pass, $passhash)) {
    //echo 'Password is valid!';
    echo json_encode($result[0]);
} else {
    echo 'Invalid password.';
}

//echo json_encode($result);

?>

